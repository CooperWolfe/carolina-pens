import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppComponent } from './app.component'
import { HeaderComponent } from './header/header.component';
import { MissionStatementComponent } from './mission-statement/mission-statement.component';
import { StaffComponent } from './staff/staff.component'
import { SuiModule } from 'ng2-semantic-ui';
import { FooterComponent } from './footer/footer.component'


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MissionStatementComponent,
    StaffComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    SuiModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
