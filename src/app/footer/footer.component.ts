import { Component } from '@angular/core'

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.styl']
})
export class FooterComponent {
  email = 'wolfewebdevelopment@gmail.com'
  logo = 'https://firebasestorage.googleapis.com/v0/b/carolina-pens.appspot.com/o/logo.png?alt=media&token=2085028c-1b98-40bf-b5ac-736ffa8ec8f7'
}
