import { Component } from '@angular/core'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.styl']
})
export class HeaderComponent {

  imageUrl = 'https://firebasestorage.googleapis.com/v0/b/carolina-pens.appspot.com/o/paper.png?alt=media&token=2960a117-47e4-460b-9127-d875b4b18069'

}
