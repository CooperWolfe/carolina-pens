import { Component } from '@angular/core'

@Component({
  selector: 'app-mission-statement',
  templateUrl: './mission-statement.component.html',
  styleUrls: ['./mission-statement.component.styl']
})
export class MissionStatementComponent {

  missionStatement = 'Carolina Pens has been representing voices native to the Carolinas since 2016. Our clients produce works rich in culture and a sense of place. Our agents have placed works of poetry, historical fiction, as well as biographical accounts. Our market includes but is not limited to journals, magazines, and editorials. Please feel free to query our agents individually based on their specialties.'

}
