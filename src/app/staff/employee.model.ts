export class Employee {

  constructor(
    public readonly position: string,
    public readonly firstname: string,
    public readonly lastname: string,
    public readonly bio: string,
    public readonly imageUrl: string,
    public readonly email: string,
    public readonly phone: string
  ) {}

}
