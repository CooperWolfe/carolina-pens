import { Component } from '@angular/core'
import { Employee } from './employee.model'

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.styl']
})
export class StaffComponent {

  private joshua = new Employee(
    'Executive', 'Joshua', 'Hutton',
    'As executive director of Carolina Pens I steer my agents toward the most original and capturing voices. We are open to most fiction, nonfiction, and poetry so long as it fulfills our mission to represent the pens of the Carolinas. Which means any and all work we represent should be rich in culture truly our own, and should have a strong sense of place. From the Upstate of SC? Check out Hub City Writer’s Project.',
    'https://firebasestorage.googleapis.com/v0/b/carolina-pens.appspot.com/o/joshua.jpg?alt=media&token=31c89216-5ae3-4a50-97e0-c0edc4fb6a07',
    'cpjoshhutton@gmail.com', '+18648149963'
  )
  private carson = new Employee(
    'Agent', 'Carson', 'Lee',
    'I have been working closely alongside the upstate’s finest up and coming poets and fiction writers for three years. If you have a unique voice and something original to say, then please send queries to my email. Or come find me at weekly Hub City Writer’s project gatherings.',
    'https://firebasestorage.googleapis.com/v0/b/carolina-pens.appspot.com/o/carson.jpg?alt=media&token=d9f10965-7097-4f36-89fd-ac86780dfe4f',
    'cpcarsonlee@gmail.com', '+18647067346'
  )
  private william = new Employee(
    'Agent', 'William', 'Jones',
    'My specialty is historical fiction but I am also interested in biographies. Please send queries to my email. I do not want to receive poetry.',
    'https://firebasestorage.googleapis.com/v0/b/carolina-pens.appspot.com/o/william.jpg?alt=media&token=6b74aa6e-4be0-4080-95df-98bdb1159c8d',
    'cpwilliamjones@gmail.com', '+18649784322'
  )

  backgroundUrl = 'https://firebasestorage.googleapis.com/v0/b/carolina-pens.appspot.com/o/blots.png?alt=media&token=3512fec0-278b-40c6-a707-95319829c8bf'
  staff: Employee[] = [this.joshua, this.carson, this.william]
  hover: boolean[] = [ false, false, false ]

}
